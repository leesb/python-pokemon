# python-pokemon

## ENV

python -m venv env

`. env/bin/activate`  
`pip install -r requirements.txt`

Add this line in a .env file :

`DATABASE_URL=sqlite:///db.sqlite3`

## Run server

python manage.py runserver


## To migrate and load models

`python manage.py makemigrations pokemon`

and

`python manage.py migrate`

Load fixture:

`python manage.py loaddata "fixture.json"`


# Use website

To start site:

`python manage.py runserver`

To add superadmin:

`python manage.py createsuperuser`


# Authorization

Create an accout on the website with /login url

Then you can use the api with a token on each of your requests with the header :

*Authorization: Token 223999xxxxxxxxxxxxxxxx*

You can have a token with your credentials in form-data at ```api-token-auth/```


# Deploy

Basic commands:

`heroku login`  
`git push heroku master`


To launch or not the website:

`heroku ps:scale web=1`

where:  
0 = stop / 1 = start


To start heroku website locally:  
`heroku local web`

And bash:  
`heroku run bash`