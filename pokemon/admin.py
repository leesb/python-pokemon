from django.contrib import admin

from pokemon import models
# Register your models here.

from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ['username', 'email', 'last_get', 'last_breed']
    fieldsets = UserAdmin.fieldsets + \
        ((None, {'fields': ('last_get', 'last_breed',)}), )


class PokemonAdmin(admin.ModelAdmin):
    model = models.Pokemon
    list_display = ['name', 'pk', 'user']


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(models.Pokemon, PokemonAdmin)
