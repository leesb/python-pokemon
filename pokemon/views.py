from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from django.contrib.auth import logout as django_logout
from django.contrib.auth import login as django_login
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils import timezone
from django.shortcuts import get_object_or_404
from django.http import HttpRequest

from django.views.generic import DetailView, ListView, TemplateView
from django.db import IntegrityError

from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from pokemon.serializers import UserSerializer, PokemonSerializer,\
    BreedSerializer, FeedSerializer

from pokemon import forms, models

from pokemon.services import pokeapi

from pokemon.pokemon import unique_pokemon, breed, feed

from random import shuffle

User = get_user_model()


def _check_date(last_date, now_date):
    d = now_date - last_date
    return (d.total_seconds() / 60 / 60) >= 8


def _safe_cast(val, to_type, default=None):
    try:
        return to_type(val)
    except (ValueError, TypeError):
        return default


def _check_feed_same_pokemon(pok1, pok2, pok3, pok4):
    list = [pok1, pok2, pok3, pok4]
    return len(list) == len(set(list))


def __starter_pokemon():
    ids = [1, 4, 7, 25, 133, 152, 155, 158, 252, 255, 258, 387, 390, 393, 495,
           498, 501, 650, 653, 656, 722, 725, 728]
    shuffle(ids)
    return ids


def index(request):
    return render(request, 'index.html')


def login(request):
    error = False

    if request.method == "POST":
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            # Vérifie si infos corrects
            user = authenticate(username=username, password=password)
            if user:
                django_login(request, user)
            else:
                error = True
    else:
        form = forms.LoginForm()

    return render(request, 'login_form.html', locals())


def logout(request):
    django_logout(request)
    return redirect(reverse(login))


def signUp(request):
    error = False
    starter = []
    if request.method == "POST":
        form = forms.SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            email = form.cleaned_data["email"]
            try:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                    email=email
                )
                user.save()
                if user:
                    django_login(request, user)
                    ids = __starter_pokemon()
                    starter.append(pokeapi.PokeApi.get_pokemon(user, ids[0]))
                    starter.append(pokeapi.PokeApi.get_pokemon(user, ids[1]))
                    starter.append(pokeapi.PokeApi.get_pokemon(user, ids[2]))
            except IntegrityError as e:
                error = True
    else:
        form = forms.SignUpForm()

    return render(request, 'sign_up_form.html', locals())


class UserBoard(object):
    def __init__(self, username, nb_pokedex, nb_total):
        self.username = username
        self.nb_pokedex = nb_pokedex
        self.nb_total = nb_total


class LeaderboardView(TemplateView):
    """
    Class which will load the view concerning the Leaderboard
    to display the list of users according to the number of Pokémon they have.
    """
    template_name = 'leaderboard.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_list = models.CustomUser.objects.all()
        object_list = []
        for user in user_list:
            object_list.append(UserBoard(user.username,
                                         unique_pokemon(user),
                                         len(models.Pokemon.objects.
                                             filter(user=user))))
        object_list.sort(key=lambda x: x.nb_pokedex, reverse=True)
        context['object_list'] = object_list
        return context


class PokemonListView(ListView):
    """
    Class which load the list of Pokémon the user has in his possession.
    """
    permission_classes = (IsAuthenticated,)
    model = models.Pokemon
    template_name = 'pokemon.html'

    def post(self, request, *args, **kwargs):
        apicall = AdoptPokemonViewSet.as_view({'post': 'create'})(request)
        if apicall.status_code == 400:
            return redirect(reverse('pokemon-list'))
        pk = apicall.data.get('pk')
        return redirect('pokemon-detail', pk=pk)

    def get_queryset(self):
        username = self.request.GET.get('user')
        user = self.request.user
        if len(models.CustomUser.objects.filter(username=username)) == 1:
            user = models.CustomUser.objects.get(username=username)
        return models.Pokemon.objects.order_by('pokemon_id',).filter(user=user)

    def get_context_data(self, **kwargs):
        context = super(PokemonListView, self).get_context_data(**kwargs)
        context['nb_pokemon'] = len(self.get_queryset())
        username = self.request.GET.get('user')
        user = self.request.user
        if len(models.CustomUser.objects.filter(username=username)) == 1:
            user = models.CustomUser.objects.get(username=username)
            context['can_get_pokemon'] = False
            context['can_breed_pokemon'] = False
            context['display_info'] = False
        else:
            context['display_info'] = True
            context['can_get_pokemon'] = self.request.user.last_get is None or\
                _check_date(self.request.user.last_get, timezone.now())
            context['can_breed_pokemon'] = \
                self.request.user.last_breed is None or \
                _check_date(self.request.user.last_breed, timezone.now())
        context['nb_pokedex'] = unique_pokemon(user)
        return context


class PokemonViewSet(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def list(self, request):
        """
        Get lists of our pokémon
        """
        queryset = models.Pokemon.objects.filter(user=self.request.user)
        serializer = PokemonSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Get a information for a single pokémon
        """
        queryset = models.Pokemon.objects.filter(user=self.request.user)
        user = get_object_or_404(queryset, pk=pk)
        serializer = PokemonSerializer(user)
        return Response(serializer.data)


class PokemonDetailView(DetailView):
    """
    Class loading the view to have details on a Pokémon.
    """
    permission_classes = (IsAuthenticated,)
    model = models.Pokemon
    template_name = 'pokemon_detail.html'


class Register(viewsets.ViewSet):
    """
    Class which will load the view to register on our site.
    """
    def create(self, request, format=None):
        serializer = UserSerializer(data=request.data,
                                    context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AdoptPokemonViewSet(viewsets.ViewSet):
    """
    Class loading the view to adopt a pokémon
    This can be done every 8 hours.
    """
    permission_classes = (IsAuthenticated,)

    def create(self, request, format=None):
        """
         Adopt a new random pokémon.
         This can be done every 8 hours.
         """
        if request.user.last_get is None or _check_date(request.user.last_get,
                                                        timezone.now()):
            pokemon = pokeapi.PokeApi.get_pokemon(request.user)
            serializer = PokemonSerializer(pokemon)
            request.user.last_get = timezone.now()
            request.user.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response("already get", status=status.HTTP_400_BAD_REQUEST)


class BreedPokemonView(TemplateView):
    permission_classes = (IsAuthenticated,)
    template_name = 'breed.html'

    def post(self, request, *args, **kwargs):
        apicall = BreedPokemonViewSet.as_view({'post': 'create'})(request)
        if apicall.status_code == 400:
            return redirect(reverse('pokemon-breed') +
                            "?error=" + apicall.data)
        pk = apicall.data.get('pk')
        return redirect('pokemon-detail', pk=pk)

    def get_queryset(self):
        return models.Pokemon.objects.order_by('pokemon_id',)\
            .filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(BreedPokemonView, self).get_context_data(**kwargs)
        context['object_list'] = self.get_queryset()
        context['error'] = self.request.GET.get('error')
        return context


class BreedPokemonViewSet(viewsets.ViewSet):
    """
    Class loading the view to reproduct two pokémon
    This can be done every 8 hours.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = BreedSerializer

    def create(self, request):
        """
        Breed 2 pokémon.
        This can be done every 8 hours.
        """
        serializer = BreedSerializer(data=request.data)
        if serializer.is_valid():
            pokemon_id_1 = serializer.data.get('pokemon_id_1')
            pokemon_id_2 = serializer.data.get('pokemon_id_2')
            if pokemon_id_1 == pokemon_id_2:
                return Response("You can't breed the same pokemon",
                                status=status.HTTP_400_BAD_REQUEST)
            pokemons = models.Pokemon.objects.filter(user=self.request.user)
            if (not pokemons.filter(pk=pokemon_id_1).exists() or
                    not pokemons.filter(pk=pokemon_id_2).exists()):
                return Response("One or more pokemon does not exist",
                                status=status.HTTP_400_BAD_REQUEST)
            pokemon1 = pokemons.get(pk=pokemon_id_1)
            pokemon2 = pokemons.get(pk=pokemon_id_2)
            if request.user.last_breed is None or\
                    _check_date(request.user.last_breed, timezone.now()):
                pokemon = breed(pokemon1, pokemon2)
                if pokemon is None:
                    return Response("Pokemons are incompatible",
                                    status=status.HTTP_400_BAD_REQUEST)
                serializer = PokemonSerializer(pokemon)
                request.user.last_breed = timezone.now()
                request.user.save()
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            else:
                return Response("already breed",
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response("Invalid fields",
                            status=status.HTTP_400_BAD_REQUEST)


class FeedPokemonView(TemplateView):
    permission_classes = (IsAuthenticated,)
    template_name = 'feed.html'

    def post(self, request, *args, **kwargs):
        apicall = FeedPokemonViewSet.as_view({'post': 'create'})(request)
        if apicall.status_code == 400:
            return redirect(reverse('pokemon-feed') + "?error=" + apicall.data)
        pk = apicall.data.get('pk')
        return redirect('pokemon-detail', pk=pk)

    def get_queryset(self):
        return models.Pokemon.objects.order_by('pokemon_id',)\
            .filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(FeedPokemonView, self).get_context_data(**kwargs)
        context['object_list'] = self.get_queryset()
        context['error'] = self.request.GET.get('error')
        return context


class FeedPokemonViewSet(viewsets.ViewSet):
    """
    Class loading the view to exchange 4 pokémon to receive a new one
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = FeedSerializer

    def create(self, request):
        """
        Feed pokémon to receive a new random one
        """
        serializer = FeedSerializer(data=request.data)
        if serializer.is_valid():
            pokemon_id_1 = serializer.data.get('pokemon_id_1')
            pokemon_id_2 = serializer.data.get('pokemon_id_2')
            pokemon_id_3 = serializer.data.get('pokemon_id_3')
            pokemon_id_4 = serializer.data.get('pokemon_id_4')
            if not _check_feed_same_pokemon(pokemon_id_1, pokemon_id_2,
                                            pokemon_id_3, pokemon_id_4):
                return Response("You can't feed the same pokemon",
                                status=status.HTTP_400_BAD_REQUEST)
            pokemons = models.Pokemon.objects.filter(user=self.request.user)
            if (not pokemons.filter(pk=pokemon_id_1).exists() or
                    not pokemons.filter(pk=pokemon_id_2).exists() or
                    not pokemons.filter(pk=pokemon_id_3).exists() or
                    not pokemons.filter(pk=pokemon_id_4).exists()):
                return Response("One or more pokemon does not exist",
                                status=status.HTTP_400_BAD_REQUEST)
            pokemon1 = pokemons.get(pk=pokemon_id_1)
            pokemon2 = pokemons.get(pk=pokemon_id_2)
            pokemon3 = pokemons.get(pk=pokemon_id_3)
            pokemon4 = pokemons.get(pk=pokemon_id_4)
            pokemon = feed(pokemon1, pokemon2, pokemon3, pokemon4)
            serializer = PokemonSerializer(pokemon)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response("Invalid fields",
                            status=status.HTTP_400_BAD_REQUEST)
