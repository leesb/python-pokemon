from typing import Optional, List

from pokemon.models import Pokemon, CustomUser, EggGroups
from pokemon.services.pokeapi import PokeApi
from random import randint


def __get_random_pokemon(user: CustomUser, groups: List[EggGroups])\
        -> Optional[Pokemon]:
    """return Pokemon
    user: CustomUser -> l'utilisateur qui va recevoir le pokémon
    groups: List de EggGroups -> La list dans lequel se trouve le pokémon qui
    sera recupéré.
    Fonction qui permet a l'utilisateur de récupérer un pokemon.
    Cette récupération se fait grace à la pokéApi et de façon aléatoire.
    """
    i = randint(0, len(groups) - 1)
    group = PokeApi.get_group(groups[i].pk)
    i = randint(0, len(group.pokemon_species) - 1)
    rand = group.pokemon_species[i]
    n = int(rand.url[42:-1])
    new = PokeApi.get_pokemon(user=user, i=n)
    return new


def feed(pok1: Pokemon, pok2: Pokemon, pok3: Pokemon, pok4: Pokemon)\
        -> Optional[Pokemon]:
    """ return Pokemon
    pok1...4: Pokemon -> les quatres pokémons à échanger.
    Cette fonction a pour but de sacrifier 4 pokemons pour en
    obtenir un nouveau.
    Le egg_group du nouveau pokemon est dépendant des pokemons sacrifiés.
    """
    groups = []
    groups.extend(pok1.egg_groups.all())
    groups.extend(pok2.egg_groups.all())
    groups.extend(pok3.egg_groups.all())
    groups.extend(pok4.egg_groups.all())
    pokemon = __get_random_pokemon(pok1.user, groups)
    pok1.delete()
    pok2.delete()
    pok3.delete()
    pok4.delete()
    return pokemon


def breed(pok1: Pokemon, pok2: Pokemon) -> Optional[Pokemon]:
    """ return Pokemon
    pok1 : Pokemon -> Le premier Pokémon que l'utilisateur veut reproduire
    pok2 : Pokemon -> Le dexuième Pokémon que l'utilisateur veut reproduire
    Cette fonction a pour but de reproduire deux pokemons.
    Cette fonction va comparer tout d'abord deux pokemons pour savoir
    s'ils sont compatibles.
    S'ils le sont alors cette fonction va donner un nouveau pokemon suite
    à la reproduction sinon rien.
    """
    if not pok1.is_compatible(pok2):
        return None

    if pok1.name == 'ditto':
        return __get_random_pokemon(pok1.user, pok2.egg_groups.all())

    if pok2.name == 'ditto':
        return __get_random_pokemon(pok1.user, pok1.egg_groups.all())

    female = pok1 if pok1.gender == 'female' else pok2
    return __get_random_pokemon(pok1.user, female.egg_groups.all())


def unique_pokemon(user: CustomUser) -> int:
    pokemons = list(set([p.pokemon_id for p
                         in Pokemon.objects.filter(user=user)]))
    return len(pokemons)
