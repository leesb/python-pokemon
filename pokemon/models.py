from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser

# Create your models here.


class CustomUser(AbstractUser):
    """
    Class qui décrit un utilisateur.
    last_get -> le moment où l'utilisateur a reçu son dernier pokemon
    last_breed -> le moment où l'utilisateur a fait sa dernière
    reproduction de Pokemon.
    """
    # add additional fields in here
    last_get = models.DateTimeField(null=True, blank=True)
    last_breed = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.username


class EggGroups(models.Model):
    name = models.CharField(max_length=60)

    def __eq__(self, other):
        if isinstance(other, EggGroups):
            return self.name == other.name
        return False

    def __str__(self):
        return self


class Pokemon(models.Model):
    """
    Class représentant un Pokemon
    user étant la personne à qui appartient le pokemon
    name représente le nom du pokemon
    evolution_chain étant les (sous)évolutions
    gender représente le sexe du pokemon
    egg_groups représente le groupe auquel appartient le pokemon
    image contient une affiche du pokemon
    pokemon_id étant l'id du pokemon
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE)
    name = models.CharField(max_length=60)
    evolution_chain = models.CharField(default=None, max_length=60)
    gender = models.CharField(max_length=12)
    egg_groups = models.ManyToManyField(EggGroups)
    image = models.CharField(max_length=100)
    pokemon_id = models.IntegerField()
    gif = models.CharField(max_length=100)

    def __str__(self):
        return '%u/ %s / %s' % (self.pk, self.name, self.gender)

    def is_compatible(self, pokemon):
        """return bool
        self: Pokemon -> le pokemon selectionné
        pokemon: Pokemon -> le pokemon avec qui le self sera comparé.
        Cette fonction va comparer le pokemon en question avec un autre.
        Cette comparaison va permettre de savoir si deux pokemons sont
        compatibles pour la reproduction.
        """
        no_eggs = EggGroups.objects.get_or_create(name='no-eggs')[0]
        peg = pokemon.egg_groups.all()
        seg = self.egg_groups.all()

        if (no_eggs in seg) or (no_eggs in peg):
            return False

        if self.name == 'ditto':
            if pokemon.name != self.name and no_eggs not in peg:
                return True
        else:
            if pokemon.name == 'ditto' and no_eggs not in seg:
                return True

        if self.gender == pokemon.gender:
            return False

        if self.name == pokemon.name:
            return True

        if self.evolution_chain == pokemon.evolution_chain:
            return True

        for eg in seg:
            if eg in peg:
                return True

        return False


class BreedModel(object):
    def __init__(self, **kwargs):
        for field in ('pokemon_id_1', 'pokemon_id_2'):
            setattr(self, field, kwargs.get(field, None))


class FeedModel(object):
    def __init__(self, **kwargs):
        for field in ('pokemon_id_1', 'pokemon_id_2',
                      'pokemon_id_3', 'pokemon_id_4'):
            setattr(self, field, kwargs.get(field, None))
