from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_swagger.views import get_swagger_view
from rest_framework import routers
from pokemon import views

schema_view = get_swagger_view(title='Pokemon API')

router = routers.DefaultRouter()
router.register(r'pokemon', views.PokemonViewSet, base_name='api-pokemon')
router.register(r'adopt', views.AdoptPokemonViewSet, base_name='api-adopt')
router.register(r'breed', views.BreedPokemonViewSet, base_name='api-breed')
router.register(r'feed', views.FeedPokemonViewSet, base_name='api-feed')
router.register(r'register', views.Register, base_name='api-register')


urlpatterns = [
        path('', views.index, name='index'),
        path('api/doc', schema_view),
        path('api/', include(router.urls)),
        path('login/', views.login, name='login'),
        path('logout/', views.logout, name='logout'),
        path('signup/', views.signUp, name='signUp'),
        path('pokemon/', views.PokemonListView.as_view(), name='pokemon-list'),
        path('breed/', views.BreedPokemonView.as_view(), name='pokemon-breed'),
        path('feed/', views.FeedPokemonView.as_view(), name='pokemon-feed'),
        path('leaderboard/', views.LeaderboardView.as_view(),
             name='leaderboard'),
        path('pokemon/<int:pk>/', views.PokemonDetailView.as_view(),
             name='pokemon-detail'),
        path('api/token-auth/', obtain_auth_token, name='api-token-auth'),
        path('api/token-jwt/', obtain_jwt_token, name='api-token-jwt'),
]
