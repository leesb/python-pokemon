from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser


class LoginForm(forms.Form):
    """
    Class qui va génerer les forms lorsqu'un user veut se connecter
    username -> le champs du nom de l'utilisateur
    password -> le mot de passe de l'utilisateur
    """
    username = forms.CharField(label="Username", max_length=30)
    password = forms.CharField(label="Password", widget=forms.PasswordInput)


class SignUpForm(forms.Form):
    """
    Class qui va génerer les forms lorsqu'un user veut s'inscrire sur le site
    username -> le champs du nom de l'utilisateur
    email -> email à saisir pour l'inscription
    password -> le mot de passe de l'utilisateur
    """
    username = forms.CharField(label="Username", max_length=30)
    email = forms.CharField(label="Email", max_length=100)
    password = forms.CharField(label="Password", widget=forms.PasswordInput)


# Forms for custom User

class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username', 'email', 'last_get')


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'last_get')
