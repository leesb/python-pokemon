from collections import namedtuple
from typing import Optional
from random import randint
from bisect import bisect_left
from pokemon.models import Pokemon
from pokemon.models import EggGroups
from pokemon.models import CustomUser
import requests
import json


def _json_object_hook(d):
    return namedtuple('X', d.keys())(*d.values())


__females = [line.rstrip('\n') for line in open('staticfiles/female.txt')]
__males = [line.rstrip('\n') for line in open('staticfiles/male.txt')]
__genderless = [line.rstrip('\n') for line
                in open('staticfiles/genderless.txt')]


def __index(a, x):
    i = bisect_left(a, x)
    if i != len(a) and a[i] == x:
        return i
    return -1


def _get_gender(name: str):
    if name in __genderless:
        return 'genderless'
    genders = []

    if __index(__females, name) >= 0:
        genders.append('female')
    if __index(__males, name) >= 0:
        genders.append('male')

    return genders[randint(0, len(genders) - 1)]


class PokeApi:

    BASE_URL = 'https://pokeapi.co/api/v2/'

    def get(url: str):
        try:
            request = requests.get(PokeApi.BASE_URL + url)
            request.raise_for_status()
            content = request.content
            response = json.loads(content, object_hook=_json_object_hook)
            return response
        except requests.exceptions.HTTPError as errh:
            print("Http Error:", errh)
        except requests.exceptions.ConnectionError as errc:
            print("Error Connecting:", errc)
        except requests.exceptions.Timeout as errt:
            print("Timeout Error:", errt)
        except requests.exceptions.RequestException as err:
            print("Something Else", err)
        return None

    def get_pokemon(user: CustomUser, i: int = -1) -> Optional[Pokemon]:
        if i < 0 or i > 807:
            i = randint(1, 807)

        species = PokeApi.get(f'pokemon-species/{i}')
        if species is None or species is None:
            return None
        result = Pokemon.objects.create(user=user,
                                        name=species.name,
                                        pokemon_id=i,
                                        evolution_chain=species.
                                        evolution_chain.url,
                                        gender=_get_gender(species.name),
                                        image='https://pokeres.bastionbot.org'
                                              + f'/images/pokemon/{i}.png',
                                        gif='https://projectpokemon.org/images'
                                            + '/normal-sprite/'
                                            + f'{species.name}.gif')

        result.egg_groups.set([EggGroups.objects.get_or_create(name=g.name)[0]
                               for g in species.egg_groups])
        result.save()
        return result

    def get_group(n):
        response = PokeApi.get(f'egg-group/{n}')
        return response
