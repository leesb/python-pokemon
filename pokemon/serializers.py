from rest_framework import serializers

from pokemon import models


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.CustomUser
        fields = ('url', 'username', 'email', 'last_get', 'last_breed')


class EggGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.EggGroups
        fields = ('pk', 'name')


class PokemonSerializer(serializers.ModelSerializer):
    egg_groups = EggGroupSerializer(many=True, read_only=True)

    class Meta:
        model = models.Pokemon
        fields = ('pk', 'name', 'pokemon_id', 'evolution_chain', 'gender',
                  'egg_groups', 'image', 'gif')


class BreedSerializer(serializers.Serializer):
    pokemon_id_1 = serializers.IntegerField(required=True)
    pokemon_id_2 = serializers.IntegerField(required=True)

    def create(self, validated_data):
        return models.BreedModel(**validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)
        return instance


class FeedSerializer(serializers.Serializer):
    pokemon_id_1 = serializers.IntegerField(required=True)
    pokemon_id_2 = serializers.IntegerField(required=True)
    pokemon_id_3 = serializers.IntegerField(required=True)
    pokemon_id_4 = serializers.IntegerField(required=True)

    def create(self, validated_data):
        return models.BreedModel(**validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)
        return instance
