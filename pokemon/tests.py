from django.test import TestCase
import random

# Create your tests here.
from pokemon.services.pokeapi import PokeApi

from pokemon.models import CustomUser, EggGroups
import pokemon.pokemon as op


class RequesterTestCase(TestCase):

    def setUp(self) -> None:
        self.user = CustomUser.objects.create(username='test')
        groups = ['monster', 'water1', 'bug', 'flying', 'ground',
                  'fairy', 'plant', 'humanshape', 'water3', 'mineral',
                  'indeterminate', 'water2', 'ditto', 'dragon', 'no-eggs']
        for g in groups:
            EggGroups.objects.create(name=g)

    def test_get_bulbasaur(self):
        pokemon = PokeApi.get_pokemon(self.user, 1)
        self.assertEqual('bulbasaur', pokemon.name)
        self.assertEqual(1, pokemon.pokemon_id)
        self.assertEqual('https://pokeapi.co/api/v2/evolution-chain/1/',
                         pokemon.evolution_chain)

    def test_get_mew(self):
        pokemon = PokeApi.get_pokemon(self.user, 151)
        self.assertEqual('mew', pokemon.name)

    def test_get_pokemon_0(self):
        pokemon = PokeApi.get_pokemon(self.user, 0)
        self.assertIsNone(pokemon)

    def test_get_group_0(self):
        species = PokeApi.get_group(0)
        self.assertIsNone(species)

    def test_get_group_1(self):
        species = PokeApi.get_group(1)
        self.assertIsNotNone(species)

    def test_get_pokemon_random(self):
        random.seed(0)
        pokemon = PokeApi.get_pokemon(self.user)
        self.assertEqual('empoleon', pokemon.name)

    def test_bulbasaur_compatible_with_ivysaur(self):
        b1 = PokeApi.get_pokemon(self.user, 1)
        b2 = PokeApi.get_pokemon(self.user, 2)
        b1.gender = 'male'
        b2.gender = 'female'
        self.assertTrue(b1.is_compatible(b2))

    def test_bulbasaur_not_compatible_with_mew(self):
        b1 = PokeApi.get_pokemon(self.user, 1)
        b2 = PokeApi.get_pokemon(self.user, 151)
        b1.gender = 'male'
        b2.gender = 'female'
        self.assertFalse(b1.is_compatible(b2))

    def test_mew_not_compatible_with_mew(self):
        b1 = PokeApi.get_pokemon(self.user, 151)
        b2 = PokeApi.get_pokemon(self.user, 151)
        b1.gender = 'male'
        b2.gender = 'female'
        self.assertFalse(b1.is_compatible(b2))

    def test_bulbasaur_compatible_with_ditto(self):
        b1 = PokeApi.get_pokemon(self.user, 1)
        b2 = PokeApi.get_pokemon(self.user, 132)
        self.assertTrue(b1.is_compatible(b2))

    def test_bulbasaur_not_compatible_with_dratini(self):
        b1 = PokeApi.get_pokemon(self.user, 1)
        b2 = PokeApi.get_pokemon(self.user, 147)
        b1.gender = 'male'
        b2.gender = 'female'
        self.assertFalse(b1.is_compatible(b2))

    def test_male_bulbasaur_not_compatible_with_male_bulbasaur(self):
        b1 = PokeApi.get_pokemon(self.user, 1)
        b2 = PokeApi.get_pokemon(self.user, 1)
        b1.gender = 'male'
        b2.gender = 'male'
        self.assertFalse(b1.is_compatible(b2))

    def test_bulbasaur_compatible_with_charmander(self):
        b1 = PokeApi.get_pokemon(self.user, 1)
        b2 = PokeApi.get_pokemon(self.user, 4)
        b1.gender = 'male'
        b2.gender = 'female'
        self.assertTrue(b1.is_compatible(b2))

    def test_male_bulbasaur_breed_female_charmander(self):
        random.seed(0)
        b1 = PokeApi.get_pokemon(self.user, 1)
        b2 = PokeApi.get_pokemon(self.user, 4)
        b1.gender = 'male'
        b2.gender = 'female'
        baby = op.breed(b1, b2)
        self.assertEqual('larvitar', baby.name)

    def test_unique_pokemon_of_user(self):
        for i in range(1, 3):
            PokeApi.get_pokemon(self.user, i)
            PokeApi.get_pokemon(self.user, i)
        self.assertEqual(2, op.unique_pokemon(self.user))
